package com.ruoyi.web.api.event;

import org.openapi.consts.ApiConst;
import org.openapi.event.EventService;
import org.openapi.service.IDbService;
import org.openapi.vo.TableData;
import org.openapi.vo.TableQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class FlowEvent implements EventService {
    @Autowired
    private IDbService dbService;

    @Override
    public void process(Object data) {
        //审批之后同步更新关联的业务状态
        Map updateData = (Map) data;
        if(!updateData.containsKey("check_status") || ApiConst.STATUS_ENABLED.equalsIgnoreCase(updateData.get("check_status").toString())){
            return;
        }

        //审批通过或不通过之后，更新业务状态
        TableQuery query = new TableQuery();
        query.setTable("asset_flow");
        query.addData("id", ((Map)data).get("id"));
        Map task = dbService.find(query);
        if(task == null || task.isEmpty()){
            return;
        }

        TableData table = new TableData();
        table.setTable(task.get("flow_model").toString());
        table.addData("id", task.get("data_id"));
        table.addData("check_status", updateData.get("check_status"));
        table.setIgnore(true);
        dbService.updateData(table);
    }
}
