package com.ruoyi.web.api;

import com.ruoyi.web.api.event.FlowEvent;
import com.ruoyi.web.api.event.PurchaseEvent;
import org.openapi.consts.EventType;
import org.openapi.event.EventBus;
import org.openapi.service.IApiLoaderService;
import org.openapi.event.ApiEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 启动加载api模型和配置
 */
@Component
public class ApiRunner implements ApplicationRunner {
    @Autowired
    private IApiLoaderService apiLoaderService;
    @Autowired
    private ApiEventService apiEventService;
    @Autowired
    private FlowEvent flowEvent;
    @Autowired
    private PurchaseEvent purchaseEvent;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        apiLoaderService.decideDb();
        apiLoaderService.loadModel();
        apiLoaderService.loadEvent();
        apiLoaderService.loadSql();
        apiLoaderService.loadExchange();

        apiEventService.initApiEvent();

        //审批回调，同步更新关联业务的状态
        //EventBus.on("asset_flow", EventType.AU, flowEvent);
        //采购主表回调，标记采购完成前验证是否全部入库
        //EventBus.on("asset_purchase", EventType.BU, purchaseEvent);
    }
}
