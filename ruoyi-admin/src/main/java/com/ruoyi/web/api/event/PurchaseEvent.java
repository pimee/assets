package com.ruoyi.web.api.event;

import org.openapi.common.ApiException;
import org.openapi.event.EventService;
import org.openapi.service.IDbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class PurchaseEvent implements EventService {
    @Autowired
    private IDbService dbService;

    @Override
    public void process(Object data) {
        //采购完成前，要确认明细都入库完成
        Map updateData = (Map) data;
        if(!updateData.containsKey("apply_status") || !"4".equalsIgnoreCase(updateData.get("apply_status").toString())){
            return;
        }

        //审批通过或不通过之后，更新业务状态
        List<Map<String,Object>> list = dbService.query("SELECT count(id) total FROM asset_purchase_detail WHERE amount!= ins AND purchase_id="+((Map)data).get("id"));
        if(list == null || list.isEmpty() || Integer.parseInt(list.get(0).get("total").toString()) == 0){
            return;
        }else{
            throw new ApiException("还有设备未采购完成");
        }
    }
}
