package org.openapi.service;

import org.openapi.vo.RelQuery;
import org.openapi.vo.SqlPager;
import org.openapi.vo.TableData;
import org.openapi.vo.TableQuery;

import java.util.List;
import java.util.Map;

/**
 * 数据库操作
 */
public interface IDbService {


    /**
     * 查询
     * @param sql
     * @return
     */
    public List<Map<String,Object>> query(String sql);

    /**
     * 更新
     * @param sql
     * @return
     */
    public int exec(String sql);

    /**
     * 联表分页查询
     * @param query
     * @return
     */
    public List<Map<String,Object>> query(RelQuery query);

    /**
     * 联表单条查询
     * @param query
     * @return
     */
    public Map find(RelQuery query);

    /**
     * 单表分页查询
     * @param table
     * @return
     */
    public List<Map<String,Object>> query(TableQuery table);

    /**
     * 分页查询
     * @param table
     * @return
     */
    public Map page(TableQuery table);

    /**
     * 通用分页查询
     * @param sql
     * @param params
     * @param pager
     * @return
     */
    public Map page(String sql, Map<String,Object>  params,  SqlPager pager);
    /**
     * 表单单条查询
     * @param table
     * @return
     */
    public Map find(TableQuery table);

    /**
     * 记录日志
     * @param modelId 模型
     * @param type 类型
     * @param key 主键
     * @param data 参数
     */
    public void log(Long modelId, String type, String key, Object data);

    /**
     * 填充关联数据
     * @sys_user.id 关联sys_user对象的ID值,目前只支持二级结构
     * 如："userId":"@USER.id"
     * @param retMap
     * @param dataMap
     */
    public void relData(Map<String, Map> retMap,  Map<String, Object> dataMap);

    /**
     * 自动填充值：
     * @userid() 当前用户ID
     * @username() 当前用户昵称
     * @orgid() 当前用户组织ID
     * @orgname() 当前用户组织名称
     * @now() 当前时间
     * @param dataMap
     */
    public void autoData(Map<String, Object> dataMap);

    /**
     * 插入数据
     * @param table
     * @param override 存在是否覆盖
     * @return
     */
    public int insertData(TableData table, boolean override);

    /**
     * 更新数据
     * @param table
     * @return
     */
    public int updateData(TableData table);


    /**
    * 删除数据
     * @param table
     * @return
     */
    public int deleteData(TableQuery table);
}
