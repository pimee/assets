package org.openapi.common;

import lombok.Data;
import org.openapi.utils.StrUtil;

/**
 * Api返回结构体
 */
@Data
public class ApiResult {
    private int code;
    private String msg;
    private Object data;

    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    public ApiResult()
    {
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg 返回内容
     */
    public ApiResult(int code, String msg)
    {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg 返回内容
     * @param data 数据对象
     */
    public ApiResult(int code, String msg, Object data)
    {
        this.code = code;
        this.msg = msg;
        if (StrUtil.isNotNull(data))
        {
            this.data = data;
        }
    }

    public static ApiResult success(){
        return new ApiResult(200, "操作成功");
    }
    public static ApiResult success(String msg){
        return new ApiResult(200, msg);
    }
    public static ApiResult success(Object data){
        return new ApiResult(200, "操作成功", data);
    }
    public static ApiResult error(){
        return new ApiResult(500, "操作失败");
    }
    public static ApiResult error(String msg){
        return new ApiResult(500, msg);
    }
    public static ApiResult error(Integer code, String msg){
        return new ApiResult(code, msg);
    }
}
