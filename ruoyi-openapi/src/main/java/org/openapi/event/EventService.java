package org.openapi.event;

/**
 * 事件处理
 */
public interface EventService {
    /** 响应方法 **/
    void process(Object data);
}
