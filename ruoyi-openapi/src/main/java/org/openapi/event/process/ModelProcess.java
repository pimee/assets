package org.openapi.event.process;

import org.openapi.domain.ApiModel;
import org.openapi.event.EventService;
import org.openapi.service.IApiLoaderService;
import org.openapi.service.IDbService;
import org.openapi.vo.TableQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ModelProcess implements EventService {

    @Autowired
    private IDbService dbService;
    @Autowired
    private IApiLoaderService apiLoaderService;

    @Override
    public void process(Object data) {
        TableQuery tableQuery = new TableQuery();
        tableQuery.setTable("api_model");
        tableQuery.addData("id", ((Map) data).get("id"));
        ApiModel model = ApiModel.fromMap(dbService.find(tableQuery));
        apiLoaderService.loadModel(model);
    }
}
