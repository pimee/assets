package org.openapi.event.process;

import org.openapi.event.EventService;
import org.openapi.service.IApiLoaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SqlProcess implements EventService {
    @Autowired
    private IApiLoaderService apiLoaderService;

    @Override
    public void process(Object data) {
        apiLoaderService.loadSql();
    }
}
