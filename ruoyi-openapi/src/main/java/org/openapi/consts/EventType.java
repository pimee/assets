package org.openapi.consts;

/**
 * 模型事件类型
 */
public enum  EventType {
    AI("AI","新增后"),
    BI("BI","新增前"),
    AU("AU","更新后"),
    BU("BU","更新前"),
    AD("AD","删除后"),
    BD("BD","删除前"),
    DC("DC","删除确认"),

    INSERT("INSERT","新增"),
    UPDATE("UPDATE","更新"),
    DELETE("DELETE","删除"),

    ;
    private String code;
    private String name;

    EventType(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
