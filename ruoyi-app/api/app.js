import { apiGet, apiPost, apiPut, apiDs	} from '@/api/openapi'
import request from '@/utils/request'
import config from '@/config'

export function getApps() {	
	return new Promise((resolve,reject)=>{
		return request({url: '/dev/apps',method: 'get'}).then(res => {
			if(res.data.rows){
				if(config.mode && config.mode != 'dev'){
					cacheApps(res.data.rows);
				}
				resolve(res);
			}else{
				uni.showToast({
				  title: '您没有权限',
				  icon: "error",
				  duration: 2500
				})
				reject(res);
			}			
		});
	});	
}

export function getApp(appCode) {
	uni.setNavigationBarTitle({
		title:"页面加载中"
	});
	
	if(config.mode && config.mode != 'dev'){
		let app = uni.getStorageSync("app_"+appCode);
		if(app){
			return Promise.resolve(app);
		}
	}
	
	return new Promise((resolve,reject)=>{
		apiGet({"@table":"dev_app",code:appCode}).then(res => {
			if(res.data){
				uni.setNavigationBarTitle({
					title:res.data.name
				});
				
				if(config.mode && config.mode != 'dev'){
					uni.setStorage({
						key: "app_"+appCode,
						data: res
					});
				}
				resolve(res);
			}else{
				uni.showToast({
				  title: '页面不存在',
				  icon: "error",
				  duration: 2500
				})
				setTimeout(function(){
					uni.navigateBack();
				},1500)
			}			
		});
	});	
}

export function getForm(appId) {
	return apiGet({"@table":"dev_app_form","app_id":appId, "@pager":{limit:100,order:"sorts ASC"}});
	
	let form = uni.getStorageSync("app_form_"+appId);
	if(form){
		return Promise.resolve(form);
	}
	return new Promise((resolve,reject)=>{
		apiGet({"@table":"dev_app_form","app_id":appId, "@pager":{limit:100,order:"sorts ASC"}}).then(res => {
			uni.setStorage({
				key: "app_form_"+appId,
				data: res
			});
			resolve(res)
		});
	});
}