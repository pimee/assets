export default {
  setData: function (dsData, set) {
    let option = set ? Object.assign(this.options, set) : { ...this.options };
    option.series[0].data = [];
    option.xAxis.data = [];
    for (var i = 0; i < dsData.length; i++) {
      option.xAxis.data.push(dsData[i]['name'])
      option.series[0].data.push(dsData[i]['value'])
    }
    return option;
  },

  options: {
    xAxis: {
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data: [150, 230, 224, 218, 135, 147, 260],
        type: 'line'
      }
    ]
  }
}