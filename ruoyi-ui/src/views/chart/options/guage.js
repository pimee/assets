export default {
    setData: function (dsData, set) {
        let option = set ? Object.assign(this.options, set) : { ...this.options };
        option.series[0].data = [];
        for (var i = 0; i < dsData.length; i++) {
            option.series[0].data.push(dsData[i])
        }
        return option;
    },

    options: {
        tooltip: {
            formatter: '{a} <br/>{b} : {c}%'
        },
        series: [
            {
                type: 'gauge',
                detail: {
                    formatter: '{value}'
                },
                data: [
                    {
                        value: 50,
                        name: 'SCORE'
                    }
                ]
            }
        ]
    }
}